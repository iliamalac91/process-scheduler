/*
CPU Scheduler Project
Scheduling algorithms used: FCFS, HPF, RR.
MIT License Agreement
Made my Ilia Malac
March 25, 2020.
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

typedef struct Process {
    int id, burstTime, original_burst_time, priority, wait, turnAroundTime, timeSlices;
} Process;

typedef struct Queue
{
    int front, rear, size;
    unsigned capacity;
    Process* processes;
} Queue;

Queue* createQueue(unsigned capacity)
{
    Queue* queue = (Queue*) malloc(sizeof(Queue));
    queue->capacity = capacity;
    queue->front = queue->size = 0;
    queue->rear = capacity - 1;
    queue->processes = (Process*) malloc(queue->capacity * sizeof(Process));
    return queue;
}

int isFull(Queue* queue)
{
    return (queue->size == queue->capacity);
}

int isEmpty(Queue* queue)
{
    return (queue->size == 0);
}

void enqueue(Queue* queue, Process process)
{
    if (isFull(queue)) 
        return;
    queue->rear = (queue->rear + 1)%queue->capacity;
    queue->processes[queue->rear] = process;
    queue->size += 1;
}

Process dequeue(Queue* queue)
{
    if (isEmpty(queue))
    {
        Process emptyProcess = { 0,0,0,0,0 };
        return emptyProcess;
    }
    Process item = queue->processes[queue->front];
    queue->front = (queue->front + 1)%queue->capacity;
    queue->size -= 1;
    return item;
}

Process front(Queue* queue)
{
    if (isEmpty(queue))
    {
        Process emptyProcess = { 0,0,0 };
        return emptyProcess;
    }
    return queue->processes[queue->front];
}

Process rear(Queue* queue)
{
    if (isEmpty(queue))
    {
        Process emptyProcess = { 0,0,0 };
        return emptyProcess;
    }
    return queue->processes[queue->rear];
}

void printFrontAndRearProcesses(Queue* queue) {
    Process temp_process;
    temp_process = front(queue);
    printf("Front item is %d %d %d\n", temp_process.id, temp_process.burstTime, temp_process.priority);
    temp_process = rear(queue);
    printf("Rear item is %d %d %d\n", temp_process.id, temp_process.burstTime, temp_process.priority);
}

void sortInHPF_Order(Queue* queue)
{
    Queue* sortedQueue = createQueue(queue->capacity);
    int current_priority = 0;
    while (!isEmpty(queue))
    {
        Process temp;
        int i;
        for (i = 0; i < queue->size; i++)
        {
            temp = dequeue(queue);
            (temp.priority <= current_priority) ? enqueue(sortedQueue, temp) : enqueue(queue, temp);
        }
        current_priority += 1;
    }
    *queue = *sortedQueue;
    free(sortedQueue);
}

void printProcessList(Queue* queue, const char* orderType)
{
    if (strcmp(orderType, "fcfs") == 0) {
        printf("Process list in FCFS order as entered:\n");
    }
    else if (strcmp(orderType, "hpf") == 0) {
        sortInHPF_Order(queue);
        printf("Process list in HPF order:\n");
    }
    else if (strcmp(orderType, "rr") == 0) {
        printf("Process list for RR in order entered:\n");
    }
    else {
        printf("No order chosen, printing in order as entered:\n");
    }

    Process temp_process;
    int i;
    for (i = 0; i < queue->size; i++) {
        temp_process = queue->processes[i];
        printf("%2d %2d %2d\n", temp_process.id, temp_process.burstTime, temp_process.priority);
    }
    printf("End of list.\n\n");
}

void copyQueue(Queue* sourceQueue, Queue* destinationQueue)
{
    int i;
    for (i = 0; i < sourceQueue->size; i++)
    {
        destinationQueue->front = sourceQueue->front;
        destinationQueue->rear = sourceQueue->rear;
        destinationQueue->size = sourceQueue->size;
        destinationQueue->processes[i].id = sourceQueue->processes[i].id;
        destinationQueue->processes[i].burstTime = sourceQueue->processes[i].burstTime;
        destinationQueue->processes[i].original_burst_time = sourceQueue->processes[i].original_burst_time;
        destinationQueue->processes[i].priority = sourceQueue->processes[i].priority;
        destinationQueue->processes[i].wait = sourceQueue->processes[i].wait;
        destinationQueue->processes[i].turnAroundTime = sourceQueue->processes[i].turnAroundTime;
        destinationQueue->processes[i].timeSlices = sourceQueue->processes[i].timeSlices;
    }
}

void printRoundRobinTurnAroundTime(Queue* queue, int time_quantum, int overhead)
{
    int i, id = 0, turn_around_time = 0, burst_time = 0, time_slices = 0, queue_size = 0, sum_of_all_TAT = 0;
    float throughput = 0.0, average_TAT = 0, total_TAT = 0.0;

    queue_size = queue->size;

    for (i = 0; i < queue->size; i++)
    {
        id = queue->processes[i].id;
        turn_around_time = queue->processes[i].turnAroundTime;
        burst_time = queue->processes[i].original_burst_time;
        time_slices = queue->processes[i].timeSlices;
        sum_of_all_TAT += queue->processes[i].turnAroundTime;
        printf("RR TA time for finished p%d = %d, needed: %d ms, and: %d time slices.\n", id, turn_around_time, burst_time, time_slices);
    }
    total_TAT = (float)queue->processes[i - 1].turnAroundTime;
    throughput = (float)queue_size / total_TAT;
    printf("RR Throughput, %d p, with q: %d, o: %d, is: %.4f p/ms, or %d p/us\n", queue_size, time_quantum, overhead, throughput, (int)throughput * 1000);
    average_TAT = (float)sum_of_all_TAT / (float)queue_size;
    printf("Average RR TA, %d p, with q: %d, o: %d, is: %.4f\n\n", queue_size, time_quantum, overhead, average_TAT);
}

void calculateRoundRobinProcessValues(Queue* readyQueue, int timeQuantum, int overhead)
{
    Queue* tempQueue = createQueue(readyQueue->capacity);
    Process temp_process;
    int currentTime = 0;

    while (!isEmpty(readyQueue)) 
    {
        temp_process = dequeue(readyQueue);
        temp_process.timeSlices += 1;
        temp_process.wait = currentTime;
        currentTime += (temp_process.burstTime < timeQuantum) ? temp_process.burstTime : timeQuantum;
        temp_process.burstTime -= (temp_process.burstTime < timeQuantum) ? temp_process.burstTime : timeQuantum;
        temp_process.turnAroundTime = currentTime;
        currentTime += overhead;

        (temp_process.burstTime == 0) ? enqueue(tempQueue, temp_process) : enqueue(readyQueue, temp_process);
    }
    *readyQueue = *tempQueue;
    free(tempQueue);
}

void roundRobin(Queue* readyQueue, Queue* jobQueue, int maxTimeQuantum)
{
    printProcessList(readyQueue, "rr");

    int currentTimeQuantum;
    for (currentTimeQuantum = 1; currentTimeQuantum <= maxTimeQuantum; currentTimeQuantum++)
    {
        int currentOverhead;
        for (currentOverhead = 0; currentOverhead <= currentTimeQuantum; currentOverhead++)
        {
            printf("preemptive RR schedule, quantum = %d overhead = %d\n", currentTimeQuantum, currentOverhead);
            calculateRoundRobinProcessValues(readyQueue, currentTimeQuantum, currentOverhead);
            printRoundRobinTurnAroundTime(readyQueue, currentTimeQuantum, currentOverhead);
            copyQueue(jobQueue, readyQueue);
        }
    }
    printf(" <><> end preemptive RR schedule <><>\n\n");
}

void printTurnAroundTime(Queue* queue)
{
    int i;
    float avg_tat = 0.0, last_tat = 0.0, sum_of_all_tat = 0.0, queue_size = 0.0, throughput = 0.0;
    for (i = 0; i < queue->size; i++)
    {
        printf("fcfs turn-around time for p%d = %d\n", queue->processes[i].id, queue->processes[i].turnAroundTime);
        sum_of_all_tat += (float)queue->processes[i].turnAroundTime;
    }
    queue_size = (float)queue->size;
    avg_tat = sum_of_all_tat / queue_size;
    printf("average turn-around for %d procs = %.4f\n", queue->size, avg_tat);
    last_tat = (float)queue->processes[i - 1].turnAroundTime;
    throughput = queue_size / last_tat;
    printf("fcfs throughput for %d procs = %.4f procs/ms\n", queue->size, throughput);
}

void printWait(Queue* queue)
{
    int i;
    float avg_wait = 0.0, total_wait = 0.0;
    for (i = 0; i < queue->size; i++)
    {
        printf("fcfs wait of p%d = %d\n", queue->processes[i].id, queue->processes[i].wait);
        total_wait += queue->processes[i].wait;
    }
    avg_wait = total_wait / (float)queue->size;
    printf("average wait for %d procs = %.4f\n", queue->size, avg_wait);
}

void calculateProcessValues(Queue* readyQueue)
{
    Queue* temp = createQueue(readyQueue->capacity);
    Process temp_process;
    int currentTime = 0;

    while (!isEmpty(readyQueue))
    {
        temp_process = dequeue(readyQueue);
        temp_process.wait = currentTime;
        currentTime += temp_process.burstTime;
        temp_process.turnAroundTime = currentTime;
        temp_process.burstTime -= temp_process.burstTime;

        (temp_process.burstTime == 0) ? enqueue(temp, temp_process) : enqueue(readyQueue, temp_process);
    }
    *readyQueue = *temp;
    free(temp);
}

void HPF(Queue* readyQueue)
{
    printProcessList(readyQueue, "hpf");
    calculateProcessValues(readyQueue);
    printWait(readyQueue);
    printTurnAroundTime(readyQueue);
    printf(" <><> end HPF <><>\n\n");
}

//First Come First Serve process schedueling
void FCFS(Queue* readyQueue)
{
    printProcessList(readyQueue, "fcfs");
    calculateProcessValues(readyQueue);
    printWait(readyQueue);
    printTurnAroundTime(readyQueue);
    printf(" <><> end FCFS <><>\n\n");
}

void getUserProcessesListValues(Queue* queue)
{
    printf("Enter triples: process id, time in ms, and priority:\n");
    printf("For example:\n");
    printf("1 12 0\n");
    printf("3  9 1\n");
    printf("2 99 2\n");
    /*
    printf("process 1 needs 12 ms and has priority 0, very high,\n");
    printf("process 3 needs  9 ms and has priority 1,\n");
    */
    printf("and so on ...\n");

    unsigned i;
    int id_temp, burstTime_temp, priority_temp;
    for (i = 0; i < queue->capacity; i++)
    {
        scanf_s("%d %d %d", &id_temp, &burstTime_temp, &priority_temp);
        Process temp_process = { id_temp, burstTime_temp, burstTime_temp, priority_temp };
        enqueue(queue, temp_process);
    }
    printf("\n");
}

int main()
{
    int numberOfProcesses = 0;
    int timeQuantum = 5;

    FILE* fptr;
    char mode [7];

    printf(" <><> Process Scheduler Simulator Program <><>\n\n");
    printf("Choose manual or auto mode, type m/manual or a/auto:\n");
    scanf_s("%s", mode);

    if (strcmp(mode, "m") == 0 || strcmp(mode, "manual") == 0)
    {
        printf("Enter the amount of processes: ");
        scanf_s("%d", &numberOfProcesses);
    }
    else if (strcmp(mode, "a") == 0 || strcmp(mode, "auto") == 0)
    {

    }

    Queue* jobQueue = createQueue(numberOfProcesses);
    Queue* readyQueue = createQueue(numberOfProcesses);
    
    getUserProcessesListValues(jobQueue);

    copyQueue(jobQueue, readyQueue);
    FCFS(readyQueue);

    copyQueue(jobQueue, readyQueue);
    HPF(readyQueue);

    copyQueue(jobQueue, readyQueue);
    roundRobin(readyQueue, jobQueue, timeQuantum);

    return 0;
}